package com.proyecto.ecommercecat;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

import com.proyecto.ecommercecat.model.Usuario;
import com.proyecto.ecommercecat.repository.UsuarioRepository;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SprintEcommercecatApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintEcommercecatApplication.class, args);
	}

	//TODO borrar... método de prueba
	@Bean
	public CommandLineRunner demo(UsuarioRepository repo) {
		return (args) -> {
			repo.save(new Usuario("Nata", "nata@catstoree.co", "Carrera x calle", "3208080080"));
		};
	}
}
