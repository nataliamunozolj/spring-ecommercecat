package com.proyecto.ecommercecat.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyecto.ecommercecat.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	Usuario buscarPorEmail(String email);
}
