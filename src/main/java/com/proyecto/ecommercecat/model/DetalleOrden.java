package com.proyecto.ecommercecat.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "detalles")
public class DetalleOrden {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDetalleOrden; 
	private String nombre;
	private Double cantidad;
	private Double descuento;	
	private Double total;
	
	@OneToOne
	private Orden orden; 
	
	@OneToMany
	private Producto producto; 
	
	public DetalleOrden() {
		
	}

	public DetalleOrden(Integer idDetalleOrden, String nombre, double cantidad, double descuento, double total) {
		super();
		this.idDetalleOrden = idDetalleOrden;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.descuento = descuento;
		this.total = total;
	}

	public Integer getIdDetalleOrden() {
		return idDetalleOrden;
	}

	public void setIdDetalleOrden(Integer id) {
		this.idDetalleOrden = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
	

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "DetalleOrden [idDetalleOrden=" + idDetalleOrden + ", nombre=" + nombre + ", cantidad=" + cantidad + ", descuento=" + descuento
				+ ", total=" + total + "]";
	}
	
	
}
